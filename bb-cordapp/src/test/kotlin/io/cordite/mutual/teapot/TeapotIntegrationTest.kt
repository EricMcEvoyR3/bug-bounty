/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.mutual.teapot

import io.bluebank.braid.client.BraidClient
import io.cordite.bugbounty.BountyApi
import io.cordite.commons.utils.contextLogger
import io.cordite.dao.core.DaoState
import io.cordite.dao.proposal.ProposalLifecycle
import io.cordite.dao.proposal.ProposalState
import io.cordite.dgl.corda.LedgerApi
import io.cordite.test.utils.*
import io.vertx.ext.unit.junit.VertxUnitRunner
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.StartedMockNode
import org.junit.AfterClass
import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith

class TestNode(node: StartedMockNode, braidPortHelper: BraidPortHelper) {

  private val bountyBraidClient: BraidClient
  private val dglBraidClient: BraidClient

  val party: Party = node.info.legalIdentities.first()
  val bountyApi: BountyApi
  private val dglApi: LedgerApi

  init {
    bountyBraidClient = BraidClientHelper.braidClient(braidPortHelper.portForParty(party), "bounty", "localhost")
    bountyApi = bountyBraidClient.bind(BountyApi::class.java)
    dglBraidClient = BraidClientHelper.braidClient(braidPortHelper.portForParty(party), "ledger", "localhost")
    dglApi = dglBraidClient.bind(LedgerApi::class.java)
  }

  fun shutdown() {
    bountyBraidClient.close()
    dglBraidClient.close()
  }

}

@RunWith(VertxUnitRunner::class)
class TeatpotIntegrationTest {

  companion object {
    private val log = contextLogger()
    private const val casaDaoBase = "casaDao"

    private lateinit var network: MockNetwork

    private val braidPortHelper = BraidPortHelper()

    private lateinit var proposer: TestNode
    private lateinit var newMember: TestNode
    private lateinit var anotherMember: TestNode
    private lateinit var notaryName: CordaX500Name

    private var salt = 0

    @BeforeClass
    @JvmStatic
    fun setup() {

      braidPortHelper.setSystemPropertiesFor(proposerName, newMemberName, anotherMemberName)
      network = MockNetwork(cordappPackages = listOf("io.cordite"))

      proposer = TestNode(network.createPartyNode(proposerName), braidPortHelper)
      newMember = TestNode(network.createPartyNode(newMemberName), braidPortHelper)
      anotherMember = TestNode(network.createPartyNode(anotherMemberName), braidPortHelper)

      network.runNetwork()

      notaryName = network.defaultNotaryIdentity.name
    }

    @AfterClass
    @JvmStatic
    fun tearDown() {
      proposer.shutdown()
      newMember.shutdown()
      anotherMember.shutdown()
      try {
        network.stopNodes()
      } catch (e: NullPointerException) {
        log.info(e.message)
      }
    }

  }

  private val saltedDaoName = casaDaoBase + salt++

  @Test
  fun `should be able to create a dao`() {
    createDaoWithName(saltedDaoName, notaryName)
  }

  @Test
  fun `should be able to create, vote for and accept and list new teapot proposal`() {
    val originalProposalCount = proposer.bountyApi.listProposalKeys().size
    val daoState = createDaoWithName(saltedDaoName, notaryName)
    addMemberToDao(newMember, proposer, saltedDaoName)
    createAndAcceptProposalWithName(proposer, daoState, newMember)

    val keys = proposer.bountyApi.listProposalKeys()
    Assert.assertEquals("there should be only one proposal", originalProposalCount + 1, keys.size)
    val proposal = proposer.bountyApi.proposalFor(keys.first())
    Assert.assertEquals("proposal should have correct key", keys.first(), proposal.proposal.key())
  }

  private fun createAndAcceptProposalWithName(proposalProposer: TestNode, daoState: DaoState, vararg supporters: TestNode) {
    val proposal = createTeapotProposal(proposalProposer, daoState)

    supporters.forEach {
      run(network) { it.bountyApi.voteForProposal(proposal.proposal.proposalKey) }
    }

    val proposals = proposalProposer.bountyApi.teapotProposalsFor(daoState.daoKey)

    Assert.assertEquals("there should only be one proposal", 1, proposals.size)
    Assert.assertEquals("the proposal should have ${supporters.size + 1} supporters", supporters.size + 1, proposals.first().supporters.size)
    Assert.assertTrue("the proposal should have all supporters", proposals.first().supporters.containsAll(setOf(proposalProposer.party, *supporters.map { it.party }.toTypedArray())))

    val acceptedProposalLifecycle = run(network) { proposalProposer.bountyApi.acceptProposal(proposal.proposal.proposalKey) }
    Assert.assertEquals("proposal should have been accepted", ProposalLifecycle.ACCEPTED, acceptedProposalLifecycle)
  }

  private fun createTeapotProposal(proposalProposer: TestNode, daoState: DaoState): ProposalState<TeapotProposal> {
    val proposal = run(network) { proposalProposer.bountyApi.createTeapotProposal(proposalProposer.party.name, daoState.daoKey) }

    val origProposals = proposalProposer.bountyApi.teapotProposalsFor(daoState.daoKey)
    Assert.assertEquals("there should only be one proposal", 1, origProposals.size)
    Assert.assertEquals("the proposal should have one supporter", 1, origProposals.first().supporters.size)
    Assert.assertTrue("proposer should be supporter", origProposals.first().supporters.containsAll(setOf(proposalProposer.party)))
    Assert.assertEquals("keys should be the same", proposal.proposal.proposalKey, origProposals.first().proposal.proposalKey)
    return proposal
  }


  private fun addMemberToDao(newMemberNode: TestNode, proposerNode: TestNode, daoName: String, vararg extraSigners: TestNode) {
    val proposal = run(network) { newMemberNode.bountyApi.createNewMemberProposal(daoName, proposerNode.party.name) }

    Assert.assertEquals("should be 1 supporters", 1, proposal.supporters.size)
    Assert.assertTrue("member should be a supporter", proposal.supporters.contains(newMemberNode.party))

    var numberOfSupporters = 1
    extraSigners.forEach {
      run(network) { it.bountyApi.voteForProposal(proposal.proposal.proposalKey) }
      val postVote = it.bountyApi.proposalFor(proposal.proposal.proposalKey)
      Assert.assertEquals("there should be three supporters", ++numberOfSupporters, postVote.supporters.size)
    }

    // accept member proposal
    val acceptedProposalLifecycle = run(network) { newMember.bountyApi.sponsorAcceptProposal(proposal.proposal.key(), proposal.daoKey, proposerNode.party.name) }
    Assert.assertEquals("proposal should be accepted", ProposalLifecycle.ACCEPTED, acceptedProposalLifecycle)

    assertDaoStateContainsMembers(getDaoWithRetry(newMemberNode), proposerNode.party, newMemberNode.party, *extraSigners.map { it.party }.toTypedArray())
  }


  private fun createDaoWithName(daoName: String, notaryName: CordaX500Name): DaoState {
    Assert.assertEquals("there should be no casa dao at the beginning", 0, proposer.bountyApi.daoInfo(daoName).size)
    val daoState = run(network) { proposer.bountyApi.createDao(daoName, 1, false, notaryName) }
    assertDaoStateContainsMembers(getDaoWithRetry(proposer), proposer.party)
    return daoState
  }

  private fun getDaoWithRetry(testNode: TestNode): List<DaoState> {
    (1..5).forEach {
      log.info("trying to get dao list")
      val daos = testNode.bountyApi.daoInfo(saltedDaoName)
      if (daos.isNotEmpty()) {
        log.info("phew - daos returned")
        return daos
      }
      log.info("dao state not arrived yet - snoozing")
      Thread.sleep(100)
    }
    throw RuntimeException("Unable to get daos from node - are you sure this is a race condition?")
  }
}
