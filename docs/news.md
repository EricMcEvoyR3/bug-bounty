<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
# News Page

20181127 - 0858

Good morning and thanks for such a fantastic evening last night - we all had a great time - hope you did too!

As promised, here is an email with some updates in it:

 * To join corda slack, go to https://slack.corda.net - you can then browse to the #cordite and #bug-bounty channels where we will be hanging out.  We are @nimmaj, @fuzz, @bartman250 and @rickcrook there.
 * We’ve created a trello group here: https://trello.com/invite/b/ShM16hYz/67f084ad24fe5af0afe4a2a3158c2a99/tasksforeveryone. <— this is the invite. 
 * We will add a faq.md and a news.md section to the docs directory in the repository (will paste all this info there too) - please keep an eye on these.
 * Please do have a look at the branching and sharing section of docs/exercise.md
 * We will upgrade bug-bounty to a version of cordite with corda 3.3 today, which should remove a lot of network map noise - then we can start dealing with the remaining issues - will post on news when this is done
 * You can get a pdf of the week 1 slides here: https://www.dropbox.com/s/ao37tpxu3w1u7m2/BugBountyWeek1.pdf?dl=0
 * The DaoIntegrationTest is here: https://gitlab.com/cordite/cordite/blob/master/cordapps/dao-cordapp/src/test/kotlin/io/cordite/dao/integration/DaoIntegrationTest.kt - see the `should be able to issue plutus uber test` test for an example that mashes up DGL and DAO concerns
 * If you need a proper sleep, you can listen to my Digital Mutual talk from Corda Con here: https://www.youtube.com/watch?v=oo7LFN_D5V4&feature=youtu.be - this covers a lot of our thinking around decentralised governance

I’ll post on the #bug-bounty channel when we have further updates.

Please do send us feedback on the first night - what would you like to change, improve - what went well, what can we do to make sure you come back next week? :-)

And please do send us thoughts on how to coalesce the best ideas from across the teams in to the best bug bounty scheme possible!