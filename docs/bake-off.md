<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
# Bake Off

As we start the second half of the bake off, it's worth us defining the end game and re-visiting 
some of the aims/rules.

## Revisiting Rules

 * Code wins arguments
 * Docker image that we can run and test
 * KISS - honestly something simple that works is an awesome start
 * Feel free to continue to merge teams if you want to - be mindful of the above - smaller teams may be more advantageous (mythical man month)

## Bake Off Day

 * Each team will have 5 minutes to present and demo, followed by questions
 * Any presentation material must be checked in as markdown (no powerpoint)
 * Demoing the docker image and UI must be done on a machine that can display to the r3 projector
 
Bake off day will be January 21st.