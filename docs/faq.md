<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
# FAQ

We saw lots of issues last night, so we'll start to pull together a list of the most frequently found ones here.  Please
do also submit your own questions/answers

## How do I join the cordite slack channel

To join corda slack, go to https://slack.corda.net - you can then browse to the #cordite and #bug-bounty channels where 
we will be hanging out.

## Ubuntu docker requires sudo to run

You can fix this by following step 2 of the instructions [here](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04)

## Ubuntu docker-compose error msg: Docker compose: Invalid interpolation format for “environment" CORDITE_LEGAL_NAME....)

You can fix by installing latest docker-compose from: https://docs.docker.com/compose/install/#prerequisites


## I brought my node up but it can't write files to the directories under ./node

This is mostly an issue on linux boxes.  Often people run the docker-compose up script first and that creates the 
underlying directories with the root user.  However the docker image runs as cordite (it's best practice not to 
run docker as the root user) and then this can't write to the underlying files.

The fix is to ```sudo chown -R <your user>:<your group> node/```.  You can check that it has the same perms as the 
rest of the files in that dir with ```ls -l```.

## OpenJdk doesn't ship with JFX so the gui doesn't run

You'll need to fix this on your platform.  On ubuntu you can:

```bash
apt-get update && apt-get install -y openjdk-8-jdk openjfx
```

or have a look on ```https://wiki.openjdk.java.net/display/OpenJFX``` or just install oracle jdk 8!

## How do I clean up when I've got my docker environment in a mess?

See the bottom section of [Bug Bounty cordApp](bb-cordapp.md) for details.

## Where is a good place to start with writing DAO and DGL tests?

The DaoIntegrationTest is here: https://gitlab.com/cordite/cordite/blob/master/cordapps/dao-cordapp/src/test/kotlin/io/cordite/dao/integration/DaoIntegrationTest.kt - see 
the `should be able to issue plutus uber test` test for an example that mashes up DGL and DAO concerns.