<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
# Cordite Bounty Scheme
## Aims
   1. Encourage people to contribute to [Cordite project](https://gitlab.com/cordite/cordite) as measured by:
      * Increasing rate of new contributors to Cordite project
      * Increasing mean length of time contributors remain active
   2. Increases the volume and velocity of [Cordite issue backlog](https://gitlab.com/cordite/cordite/issues) as measured by:
      * Increasing rate of good quality issues being raised
      * Increasing rate of good quality issues being closed
      * Reducing mean length of time an issue is open
   3. Encourages good behaviour, as defined as the [six core principles of the Apache Way](http://www.apache.org/foundation/how-it-works.html#philosophy) :
       * collaborative software development
       * commercial-friendly standard license
       * consistently high quality software
       * respectful, honest, technical-based interaction
       * faithful implementation of standards
       * security as a mandatory feature
   4. Discourage bad behaviour, as defined as:
       * Anything that discourages good behaviour
       * Anything that reduces the volume or velocity of Cordite issue backlog
       * Cheating or gaming the scheme
   5. Follows [Apache foundation how it works](http://www.apache.org/foundation/how-it-works.html)
   6. Based on public information available through [GitLab API](https://docs.gitlab.com/ee/api/) 

## Rules
   1. MUST be MVP ready code - We will be going live!
   2. MUST be a public fork/branch of this repo:
   3. MUST extend the desktop GUI using https://tornadofx.io/ 
   4. MUST extend the Cordite libraries - https://gitlab.com/cordite/cordite
   5. MUST agree to https://www.apache.org/licenses/icla.pdf
   6. MUST keep all code in public repos at all times
   7. MUST meet the ‘Aims of Cordite Bounty Scheme’

