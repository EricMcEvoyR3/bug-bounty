<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
# Bounty exercise

The basic intention of this exercise is to get everyone:

   1.  with the code checked out
   2.  able to run a node locally (using CI built `master` image)
   3.  able to run the gui locally (using gradle magic)
   4.  able to exercise some basic functionality
   5.  able to build the node locally and run it
   6.  Branching and Sharing
   7.  Where is everyone else?
 
This is bound to go utterly wrong - we can't easily test this in R3!  So please bear with us and we'll all learn lots
while working through the issues :-)

# 1. Clone the repo

Presumably you're already here, because otherwise how can you be reading this :-)

# 2. Spin up a node

We have a ```docker-compose.yml``` file in the root directory. This by defaults pulls the CI build `master` image from [gitlab docker registry](https://gitlab.com/cordite/bug-bounty/container_registry)

We are all running behind the same firewall, so we reckon, ipv6 notwithstanding, that we probably have a few issues
to navigate here.  We're going to use [serveo](https://serveo.net) as a way of "navigating" these.  Presumably we will each
need our own port number.  So reserve one using the unbelievably shonky method of editing the wiki 
[here](https://gitlab.com/cordite/bug-bounty-template/wikis/home).  Port range between 10000-19999

You will also need to specify a name for your node.  Please use your gitlab id.  We are _bound_ to have to
do some fairly horrific things to the network map service as part of this fun and games - at least this way
we will know who we're rogering!

So for this step, broadly (in your terminal - iterm2 with ohmyzshell is awesome if you don't use it):

```bash
export CORDITE_LEGAL_NAME="O=<YOUR GITLAB USERNAME>, OU=Corda Code Club, L=London, C=GB"
export CORDITE_P2P_PORT=<YOUR RESERVED PORT>
export CORDITE_TAG=master
docker-compose pull
docker-compose up --no-build
```

At the end of this, you should have:

 * a docker node running with corda, cordite and bug bounty running in it
 * a serveo container that is mapping outside traffic to your node with a well known address
 * a new directory ```/node``` which contains your certs, db and node-info. ***DO NOT DELETE THIS!*** docker-compose will re-use 
 these each time the node starts.

Note that the corda node is listening to p2p traffic from the outside world, and it's running braid on port 8080.
The latter is so that we can talk to our node form our gui.  Speaking of which...
 
# 3. Let's point a gui at it

So the next step is to get your gui running.  You can choose whether to do this using gradle or intellij.  The 
obvious advantage of the second approach is the ability to debug.

For the moment, let's just use gradle:

```bash
./gradlew gui:run
```

This runs up a noddy gui pointing at localhost 8080, which your node should be listening to for braid.

# 4. Basic functionality of GUI

### i. Create a token

Let's start by creating a token.  

 * Click on Ledger in the drawer on the left of the gui.  
 * Click on the create token button
 * Enter a symbol (usually short) and an exponent (keep this small - say 2.  If you were defining GBP, it would be 
 stored in pence, so you would set the exponent to 2)
 * Click on create token.  You should see the token appear in the token table.
 
### ii. Issue some tokens

 * Create an account by clicking on create account, giving it a name, and clicking on the create account button
 * Now click on the issue tokens button
   * select the account you just created from the drop down
   * your token from the next dropdown
   * put some sensible amount (10000) in the amount field
   * enter a description ("happy birthday") - NB you seem to have to do this right now
   * click on issue tokens - after a second or so you should see the amount appear

### iii. Transfer some tokens 

 * Click on the transfer tokens button
   * select the from account
   * ask someone to give you their account (note that if you right click on the account you can copy the address)
   * select token, amount and description
   * hit transfer tokens
 * You might need to hit refresh, and it's worth keeping an eye on the logs, but this should transfer some tokens!
 
### iv. Create your own mutual

 * Go to the Mutuals screen
 * Click on the create dm button
   * give it a name
   * set min members to 2
   * leave strict mode off
   * click on create dm
 
You should now see a mutual on your mutuals screen
 
### v. Get your partner to join the mutual

 * The joiner should click on join dm
   * type in the mutual name
   * select the creator in the sponsor dropdown
   * click on join dm
 
This actually creates a proposal that should be on both of your nodes now.

 * Click on the proposal tab
 * Dm creator selects the proposal and clicks vote
 * DM creator re-selects (sorry!) the proposal and clicks accept
 
You should now see the mutual on the joiner's screen and 2 members on the creator's screen.

### vi. Add the economics plug in

See if you can figure out how to do this.  You need to click on the create model data proposal on the proposals tab.

(NB suspect you need to use a new token here - first bug?)

### vii. Propose to issue some tokens

See if you can figure this out too!

# 5. Build the node locally and run it

You don't need to export _all_ the variables again unless you have done something silly.  You do need to change the 
```CORDITE_TAG``` one to local, however.

The same ```docker-compose``` can be used to build a local image using the ```Dockerfile``` in the root directory.  
As you can see, this uses a cordite image as its base image, removes some of the cordite cordapps that we don't need, 
and then uploads our local cordapp.  ```gradle``` is used to build the cordApp

```bash
export CORDITE_LEGAL_NAME="O=<YOUR GITLAB USERNAME>, OU=Corda Code Club, L=London, C=GB"
export CORDITE_P2P_PORT=<YOUR RESERVED PORT>
export CORDITE_TAG=local
./gradlew clean test
docker-compose build
docker-compose up
```

# 6. Branching and Sharing

For the Bug Bounty Scheme Bake Off create a branch for your team name ```git checkout -b <TeamName>```  
Each time you ```git push``` the CI will run and create an image for your team in the 
[gitlab docker registry](https://gitlab.com/cordite/bug-bounty/container_registry)

You can run other team's nodes by:

```bash
export CORDITE_LEGAL_NAME="O=<YOUR GITLAB USERNAME>, OU=Corda Code Club, L=London, C=GB"
export CORDITE_P2P_PORT=<YOUR RESERVED PORT>
export CORDITE_TAG=<TeamName>
docker-compose pull
docker-compose up -d --no-build
./gradlew gui:run
```

```docker-compose down``` to tear down the node.

# 7. Where is everyone?

```./etc/nms/get.sh``` lists all the nodes on the network.  You can also go [here](https://nms-bb.cordite.foundation/)
though the web client has a bug in it currently.
