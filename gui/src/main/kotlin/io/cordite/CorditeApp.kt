/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite

import io.cordite.views.MainView
import javafx.application.Application
import javafx.application.Platform
import javafx.scene.image.Image
import javafx.stage.Stage
import tornadofx.*
import kotlin.system.exitProcess

class CorditeApp : App(MainView::class, Styles::class) {
  override fun start(stage: Stage) {
    super.start(stage)
    stage.width = 1000.0
    stage.height = 750.0
    addStageIcon(Image("logo-watermark-200.png"))
    stage.setOnCloseRequest {
      Platform.exit()
      exitProcess(0)
    }

  }


}

/**
 * The main method is needed to support the mvn jfx:run goal.
 */
fun main(args: Array<String>) {
  Application.launch(CorditeApp::class.java, *args)
}
