/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.views

import io.cordite.Styles
import io.cordite.viewmodel.ProposalModel
import io.cordite.viewmodel.ProposalViewModel
import javafx.geometry.Pos
import tornadofx.*

class ProposalView : View("Proposals") {
  val viewModel: ProposalViewModel by inject()

  var selectedProposal: ProposalModel? = null

  override val root = borderpane {
    addClass(Styles.mainScreen)
    top {
      stackpane {
        label(title).addClass(Styles.heading)
      }
    }
    center {
      stackpane {
        tableview(viewModel.proposalModels) {
          readonlyColumn("Name", ProposalModel::name)
          readonlyColumn("Type", ProposalModel::type)
          readonlyColumn("State", ProposalModel::state)
          readonlyColumn("# Supporters", ProposalModel::numberOfSupporters)
          readonlyColumn("Supporting", ProposalModel::supporter)
          readonlyColumn("Dao Name", ProposalModel::mutualName)
          readonlyColumn("Proposer", ProposalModel::proposedBy)

          selectionModel.selectedItemProperty().onChange {
            selectedProposal = it
          }

          smartResize()
        }
      }
    }
    bottom {
      hbox(10, Pos.CENTER) {
        addClass(Styles.content)
        menubutton("create") {
          item("Normal Proposal") {
            action {
              find<CreateNormalProposalView>().openModal()
            }
          }
          item("Model Data Proposal") {
            action {
              find<CreateEconomicsModelDataProposalView>().openModal()
            }
          }
          item("Issuance Proposal") {
            action {
              find<CreateIssuanceProposalView>().openModal()
            }
          }
        }
        button("vote") {
          action {
            runAsync {
              if (selectedProposal != null) {
                viewModel.voteForProposal(selectedProposal!!)
              }
            }
          }
        }
        button("accept") {
          action {
            if (selectedProposal != null) {
              viewModel.acceptProposal(selectedProposal!!)
            }
          }
        }
      }
    }
  }
}