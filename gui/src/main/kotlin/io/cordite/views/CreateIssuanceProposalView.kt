/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.views

import io.cordite.Styles
import io.cordite.viewmodel.*
import tornadofx.*

class CreateIssuanceProposalView : View("Create Issuance Proposal") {
  val mutualViewModel: MutualViewModel by inject()
  val proposalViewModel: ProposalViewModel by inject()
  val issuanceProposalViewModel: IssuanceProposalViewModel by inject()

  override val root = borderpane {
    addClass(Styles.mainScreen)
    top {
      stackpane {
        label(title).addClass(Styles.heading)
      }
    }

    center {
      form {
        fieldset("Issuance Proposal") {
          field("Digital Mutual") {
            combobox(issuanceProposalViewModel.mutual, mutualViewModel.mutualModels) {
              cellFormat {
                text = it.name
              }
            }
          }

          field("Token") {
            combobox(issuanceProposalViewModel.descriptor, issuanceProposalViewModel.descriptors) {
              cellFormat {
                text = it.symbol
              }
            }
          }

          field("Amount") {
            textfield(issuanceProposalViewModel.amount)
          }

        }
      }.addClass(Styles.forms)
    }

    bottom {
      stackpane {
        addClass(Styles.content)
        button("create proposal") {
          enableWhen(issuanceProposalViewModel.dirty)
          action {
            issuanceProposalViewModel.commit {
              runAsync {
                proposalViewModel.createIssuanceModelDataProposal(issuanceProposalViewModel.item)
              }
            }
            close()
          }
        }
      }
    }
  }
}