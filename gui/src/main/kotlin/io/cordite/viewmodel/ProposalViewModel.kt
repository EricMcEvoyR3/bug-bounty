/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.viewmodel

import io.cordite.dao.economics.EconomicsModelData
import io.cordite.dao.membership.MemberProposal
import io.cordite.dao.proposal.ProposalKey
import io.cordite.dao.proposal.ProposalState
import io.cordite.dgl.corda.token.TokenType
import io.cordite.model.MutualModel
import io.cordite.model.NetworkModel
import io.cordite.util.createHandler
import javafx.beans.property.Property
import javafx.beans.property.StringProperty
import tornadofx.*

class ProposalViewModel: Controller() {

  private val mutualApi = MutualModel.mutualApi

  val proposalModels = mutualApi.listProposalKeys().map{ mutualApi.proposalFor(it) }.map{ ProposalModel(it) }.observable()

  init {
    mutualApi.listenForProposalUpdates().subscribe({ updated ->
      println("pu: $updated")
      proposalModels.removeIf { it.isSameProposal(updated) }
      proposalModels.add(ProposalModel(updated))
    }, {e -> println(e)}, {println("finished")})
  }

  fun voteForProposal(proposal: ProposalModel) {
    println("voting for proposal: ${proposal.key}")
    mutualApi.voteForProposal(proposal.key).setHandler(createHandler("voted"))
  }

  fun acceptProposal(proposal: ProposalModel) {
    if (proposal.isMyNewMemberProposal()) {
      println("asking ${proposal.proposedBy} to sponsor accept proposal ${proposal.key}")
      mutualApi.sponsorAcceptProposal(proposal.key, proposal.proposalState.daoKey, proposal.proposalState.proposer.name).setHandler(createHandler("accepted"))
    } else {
      println("propose accept for proposal: ${proposal.key}")
      mutualApi.acceptProposal(proposal.key).setHandler(createHandler("accepted"))
    }
  }

  fun createNormalProposal(normalProposal: NormalProposal) {
    mutualApi.createNormalProposal(normalProposal.name, normalProposal.description, normalProposal.mutual.mutualKey).setHandler(createHandler("created normal proposal"))
  }

  fun createEconomicsModelDataProposal(proposal: EconomicsModelDataProposal) {
    mutualApi.createModelDataProposal(proposal.name, EconomicsModelData(listOf(TokenType.Descriptor(proposal.tokenName, proposal.exponent, NetworkModel.me.name))), proposal.mutual.mutualKey)
        .setHandler(createHandler("created economic model data"))
  }

  fun createIssuanceModelDataProposal(proposal: NewIssuanceProposal) {
    mutualApi.createIssuanceProposal(proposal.descriptor, proposal.amount, proposal.mutual.mutualKey).setHandler(createHandler("created issuance proposal"))
  }

}

class ProposalModel(val proposalState: ProposalState<*>) {
  val name: String get() = proposalState.name
  val type: String get() = proposalState.proposal::class.simpleName!!
  val state: String get() = proposalState.lifecycleState.name
  val mutualName: String get() = proposalState.daoKey.name
  val numberOfSupporters: Int get() = proposalState.supporters.size
  val supporter: Boolean get() = proposalState.supporters.contains(NetworkModel.me)
  val proposedBy: String get() = proposalState.proposer.name.toString()
  val key: ProposalKey = proposalState.proposal.key()

  fun isMyNewMemberProposal(): Boolean {
    val proposal = proposalState.proposal
    return (proposal is MemberProposal && proposal.type == MemberProposal.Type.NewMember && proposal.member == NetworkModel.me)
  }

  fun isSameProposal(otherProposal: ProposalState<*>):Boolean {
    return proposalState.proposal.key() == otherProposal.proposal.key()
  }
}

class NormalProposal {
  var name: String by property<String>()
  fun nameProperty() = getProperty(NormalProposal::name)

  var description: String by property<String>()
  fun descriptionProperty() = getProperty(NormalProposal::description)

  var mutual: io.cordite.viewmodel.MutualModel by property<io.cordite.viewmodel.MutualModel>()
  fun daoProperty() = getProperty(NormalProposal::mutual)
}

class NormalProposalModel: ItemViewModel<NormalProposal>(NormalProposal()) {
  val name: StringProperty = bind{ item?.nameProperty() }
  val description: StringProperty = bind{ item?.descriptionProperty() }
  val mutual: Property<io.cordite.viewmodel.MutualModel> = bind{ item?.daoProperty() }
}

class EconomicsModelDataProposal {
  var name: String by property<String>()
  fun nameProperty() = getProperty(EconomicsModelDataProposal::name)

  var tokenName: String by property<String>()
  fun tokenNameProperty() = getProperty(EconomicsModelDataProposal::tokenName)

  var exponent: Int by property<Int>()
  fun exponentProperty() = getProperty(EconomicsModelDataProposal::exponent)

  var mutual: io.cordite.viewmodel.MutualModel by property<io.cordite.viewmodel.MutualModel>()
  fun mutualProperty() = getProperty(EconomicsModelDataProposal::mutual)
}

class EconomicsModelDataModel: ItemViewModel<EconomicsModelDataProposal>(EconomicsModelDataProposal()) {
  val name: StringProperty = bind{ item?.nameProperty() }
  val tokenName: StringProperty = bind{ item?.tokenNameProperty() }
  val exponent: Property<Int> = bind{ item?.exponentProperty() }
  val mutual: Property<io.cordite.viewmodel.MutualModel> = bind{ item?.mutualProperty() }
}

class NewIssuanceProposal {
  var mutual: io.cordite.viewmodel.MutualModel by property<io.cordite.viewmodel.MutualModel>()
  fun mutualProperty() = getProperty(NewIssuanceProposal::mutual)

  var descriptor: TokenType.Descriptor by property<TokenType.Descriptor>()
  fun descriptorProperty() = getProperty(NewIssuanceProposal::descriptor)

  var amount: String by property<String>()
  fun amountProperty() = getProperty(NewIssuanceProposal::amount)
}

class IssuanceProposalViewModel: ItemViewModel<NewIssuanceProposal>(NewIssuanceProposal()) {
  val mutual: Property<io.cordite.viewmodel.MutualModel> = bind{ item?.mutualProperty() }
  val descriptor: Property<TokenType.Descriptor> = bind{ item?.descriptorProperty() }
  val amount: StringProperty = bind{ item?.amountProperty() }
  val descriptors = mutableListOf<TokenType.Descriptor>().observable()

  // surely we can do this using some combination of obervers, bindings etc...
  init {
    mutual.onChange {
      println("yay - it's changed = ${mutual.value.economicModelDataTokens()}")
      descriptors.clear()
      descriptors.addAll(mutual.value.economicModelDataTokens())
    }
  }
}