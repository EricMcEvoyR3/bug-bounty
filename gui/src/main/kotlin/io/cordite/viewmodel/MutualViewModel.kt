/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.viewmodel

import io.cordite.dao.core.DaoKey
import io.cordite.dao.core.DaoState
import io.cordite.dao.economics.EconomicsModelData
import io.cordite.dgl.corda.token.TokenType
import io.cordite.model.MutualModel
import io.cordite.model.NetworkModel
import io.cordite.util.createHandler
import javafx.beans.property.Property
import javafx.beans.property.StringProperty
import net.corda.core.identity.Party
import tornadofx.*

class MutualViewModel: Controller() {

  private val mutualApi = MutualModel.mutualApi

  val mutualModels = mutualApi.listDaos().map{ MutualModel(it) }.observable()
  val parties = NetworkModel.listParties().map{ PartyRow(it) }.observable()

  init {
    mutualApi.listenForDaoUpdates().subscribe({ updated ->
      println("du: $updated")
      mutualModels.removeIf { it.isSameDao(updated) }
      mutualModels.add(MutualModel(updated))
    }, {e -> println(e)}, {println("finished") })
  }

  fun createMutual(newMutual: NewMutual) {
    println("creating mutual ${newMutual.name} with min members ${newMutual.minimumMemberCount} and strictMode set to ${newMutual.strictMode}")
    mutualApi.createDao(newMutual.name, newMutual.minimumMemberCount, newMutual.strictMode, NetworkModel.notary.name).setHandler(createHandler("mutual created"))
  }

  fun joinMutual(joinMutual: JoinMutual) {
    println("requesting to join mutual: ${joinMutual.name} using sponsor: ${joinMutual.sponsor.name}")
    mutualApi.createNewMemberProposal(joinMutual.name, joinMutual.sponsor.party.name).setHandler(createHandler("mutual joined"))
  }
}

data class PartyRow(val party: Party) {

  val name: String get() = party.name.toString()

}

class MutualModel(private val mutualState: DaoState) {

  val key: String get() = mutualState.daoKey.uuid.toString()
  val name: String get() = mutualState.name
  val minMembers: Int get() = mutualState.membershipModelData().minimumMemberCount
  val memberCount: Int get() = mutualState.members.size
  val hasMinMembers: Boolean get() = mutualState.membershipModelData().hasMinNumberOfMembers
  val mutualKey: DaoKey get() = mutualState.daoKey  // TODO: dedup

  fun isSameDao(otherDao: DaoState): Boolean {
    return mutualState.daoKey == otherDao.daoKey
  }

  override fun toString(): String {
    return mutualState.toString()
  }

  fun economicModelDataTokens(): List<TokenType.Descriptor> {
    return mutualState.get(EconomicsModelData::class)?.issuableTokens ?: emptyList()
  }

}

class NewMutual {
  var name: String by property<String>()
  fun nameProperty() = getProperty(NewMutual::name)

  var minimumMemberCount: Int by property<Int>()
  fun minimumMemberCountProperty() = getProperty(NewMutual::minimumMemberCount)

  var strictMode: Boolean by property<Boolean>()
  fun strictModeProperty() = getProperty(NewMutual::strictMode)
}

class NewMutualModel: ItemViewModel<NewMutual>(NewMutual()) {
  val name: StringProperty = bind{ item?.nameProperty() }
  val minimumMemberCount: Property<Int> = bind{ item?.minimumMemberCountProperty() }
  val strictMode: Property<Boolean> = bind{ item?.strictModeProperty() }
}

class JoinMutual {
  var name: String by property<String>()
  fun nameProperty() = getProperty(JoinMutual::name)

  var sponsor: PartyRow by property<PartyRow>()
  fun sponsorProperty() = getProperty(JoinMutual::sponsor)
}

class JoinMutualModel: ItemViewModel<JoinMutual>(JoinMutual()) {
  val name: StringProperty = bind{ item?.nameProperty() }
  val sponsor: Property<PartyRow> = bind{ item?.sponsorProperty() }
}
